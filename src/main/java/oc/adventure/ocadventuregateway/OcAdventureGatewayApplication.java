package oc.adventure.ocadventuregateway;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OcAdventureGatewayApplication {

    public static void main(String[] args) {
        SpringApplication.run(OcAdventureGatewayApplication.class, args);
    }

}

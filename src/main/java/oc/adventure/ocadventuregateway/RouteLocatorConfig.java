package oc.adventure.ocadventuregateway;

import org.springframework.cloud.gateway.route.RouteLocator;
import org.springframework.cloud.gateway.route.builder.RouteLocatorBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class RouteLocatorConfig {

    private final static String EUREKA_URI = "http://eureka:8761";
    private final static String AUTH_URI = "http://auth:9999";

    @Bean
    public RouteLocator customRouteLocator(RouteLocatorBuilder builder) {
        return builder.routes()
                .route("eureka-api", r -> r.path("/eureka/api/{segment}")
                        .filters(f ->f.setPath("/eureka/{segment}"))
                        .uri(EUREKA_URI)
                         )
                .route(r -> r.path("/eureka/web")
                        .filters(f -> f.setPath("/"))
                        .uri(EUREKA_URI)
                        .id("eureka-web-start"))
                .route(r -> r.path("/eureka/admin")
                        .filters(f -> f.setPath("/admin"))
                        .uri(EUREKA_URI)
                        .id("spring-admin"))
                .route(r -> r.path("/eureka/**")
                        .uri(EUREKA_URI)
                        .id("eureka-web-other"))
                .route(r -> r.path("/auth/api/v1/ms/auth/login/")
                        .filters(f -> f.setPath("/api/v1/ms/auth/login/"))
                        .uri(AUTH_URI)
                        .id("auth-uri"))
                .build();
    }
}
